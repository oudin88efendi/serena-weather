//
//  ViewController.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//


import UIKit;
import CoreLocation;


class ViewController: UIViewController
{
	
	var polarDayIcon:UIImage? = nil;
	var polarNightIcon:UIImage? = nil;
	
	
	//MARK: Outlets
	//@IBOutlet weak var datePicker: UIDatePicker!
	@IBOutlet weak var locationsList: UICollectionView!;
	@IBOutlet weak var menuButton: UIButton!
	@IBOutlet weak var polarDayNightLabel: UILabel!
	@IBOutlet weak var polarDayNightImage: UIImageView!
	@IBOutlet weak var polarDayNightStackView: UIStackView!
	@IBOutlet weak var currentDateControl: FormattedDateControl!
	@IBOutlet weak var locationDescription: UILabel!
	@IBOutlet weak var locationLocalTime: UILabel!
	@IBOutlet weak var weatherIcon: UIImageView!
	@IBOutlet weak var weatherSummary: UILabel!
	@IBOutlet weak var busyIndicator: UIActivityIndicatorView!
	@IBOutlet weak var mainStack: UIStackView!
	@IBAction func showDateControl(_ sender: UITapGestureRecognizer)
	{
		//datePicker.isHidden = false;
	}

	@IBAction func dateChanged(_ sender: UIDatePicker) {
	}
    
	@IBAction func showSettingsSidebar(_ sender: UIButton)
	{
		self.revealViewController().revealToggle(menuButton);
	}
	
   
    @IBAction func findLocation(_ sender: Any) {
        self.revealViewController().revealToggle(self);
        SWglobal.mainModel.NavigateToFindLocation();
    }
    
	@IBAction func showPrevDay(_ sender: UISwipeGestureRecognizer)
	{
		SWglobal.mainModel.currentLocation?.prevDay();
		setLocationDay(SWglobal.mainModel.currentLocation!.currentDay!);
	}
	
	@IBAction func showNextDay(_ sender: UISwipeGestureRecognizer)
	{
		SWglobal.mainModel.currentLocation?.nextDay();
		setLocationDay(SWglobal.mainModel.currentLocation!.currentDay!);
	}
	
	
    @IBAction func capture(_ sender: Any) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        var imagesToShare = [AnyObject]()
        imagesToShare.append(image!)
        
        let activityViewController = UIActivityViewController(activityItems: imagesToShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        present(activityViewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad()
	{
		super.viewDidLoad();
		
		if self.revealViewController() != nil
		{
			self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
		}
		
		SWglobal.mainModel.navigateToFindLocationDelegate = self.navigateToFindLocation;
		SWglobal.mainModel.updateViewForLocationDelegate = self.updateViewForLocation;
		
		polarDayNightStackView.isHidden = true;
		
		weatherSummary.numberOfLines = 0;
		weatherSummary.lineBreakMode = NSLineBreakMode.byWordWrapping;
		
		let bundle = Bundle(for: type(of: self));
		
		polarDayIcon = UIImage(named: "polar_day", in: bundle, compatibleWith: self.traitCollection)!;
		polarNightIcon = UIImage(named: "polar_night", in: bundle, compatibleWith: self.traitCollection);

		SWglobal.mainModel.getLocations(delegate: setLocation);
	
		locationsList.allowsSelection = true;
		locationsList.dataSource = SWglobal.mainModel.dataSource!;
		locationsList.delegate = SWglobal.mainModel.dataSource!;
		locationsList.reloadData();
		
		
		getCurrentLocation();
	}
	
	func navigateToFindLocation()
	{
		performSegue(withIdentifier: "showMapSegue", sender: nil);
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?)
	{
		
		if segue.identifier == "showMapSegue"
		{
			
			if let mapViewController = segue.destination as? MapViewController
			{
				if (SWglobal.mainModel.currentLocation != nil)
				{
					mapViewController.ininitalLocation = SWglobal.mainModel.currentLocation!;
				}
			}
		}
	}
	
	
	
	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated);
		// Hide the navigation bar for current view controller
		self.navigationController?.isNavigationBarHidden = true;
	}
	
	override func viewWillDisappear(_ animated: Bool)
	{
		super.viewWillDisappear(animated);
		// Show the navigation bar on other view controllers
		self.navigationController?.isNavigationBarHidden = false;
	}
	
	func updateViewForLocation(index: Int?)
	{
		DispatchQueue.main.async(execute:
		{
			self.setLocationView();
			
			if (SWglobal.mainModel.currentLocation != nil)
			{
				self.setLocation(SWglobal.mainModel.currentLocation!);
			}
			
			if (index != nil)
			{

			
				self.locationsList.reloadData();
				self.locationsList.selectItem(at: IndexPath(row: index!, section: 0), animated: true, scrollPosition: .left);
			}
			
		});
		

	}
	
	func getCurrentLocation()
	{
		setBusyView();
		
		SWglobal.mainModel.getLocation
		{	result in
			
				if (result == true)
				{
				}
				else
				{
					// there was an error that should be handled
				}
		}

	}
	
	func setLocationView()
	{
		mainStack.isHidden = false;
		busyIndicator.stopAnimating();
	}
	
	func setBusyView()
	{
		mainStack.isHidden = true;
		busyIndicator.startAnimating();
	}
	

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	
	func setLocation(_ location: LocationModel)
	{
		SWglobal.mainModel.currentLocation = location;
		self.locationDescription.text = location.name;
		self.locationLocalTime.text = location.currentLocalTime!;
		setLocationDay(location.currentDay!);
	}
	
	func setFetchingWeather()
	{
		weatherSummary.text = L10n.checkingForecast;
		weatherIcon.image = getWeatherIcon("na");
	}
	
	func setWeatherUnavailable()
	{
		weatherSummary.text = L10n.forecastUnavailable;
		weatherIcon.image = getWeatherIcon("na");
	}
	
	func getWeatherIcon(_ icon: String) -> UIImage?
	{
		let bundle = Bundle(for: type(of: self));
		var weatherIcon = UIImage(named: icon, in: bundle, compatibleWith: self.traitCollection);
		
		if (weatherIcon == nil)
		{
			weatherIcon = UIImage(named: "default", in: bundle, compatibleWith: self.traitCollection);
		}
		
		return weatherIcon;
	}
	
	func setWeather(_ weather: WeatherModel)
	{
		weatherSummary.text = weather.temperatureInfo + "\n" + weather.summary;
		weatherIcon.image = getWeatherIcon(weather.icon);
	}
	
	func setWeather()
	{
		let location = SWglobal.mainModel.currentLocation;
		
		if (location == nil)
		{
			return;
		}
		
		if (!location!.weatherForeCastIsKnown)
		{
			//fetch weather
			setFetchingWeather();
			location!.fetchWeatherForecast
			{
				DispatchQueue.main.async(execute: 
				{
						self.setWeather();
				});
			}
		}
		else if (location!.containsWeather(for: location!.currentDay!.date))
		{
			setWeather(location!.getWeather(for: location!.currentDay!.date)!);
		}
		else
		{
			setWeatherUnavailable();
		}

	}
	
	
	func setLocationDay(_ info: DayModel)
	{
		
		self.currentDateControl.date = info.date;
		
		if (info.type == .regular)
		{
			polarDayNightStackView.isHidden = true;
			
		}
		else
		{
			polarDayNightStackView.isHidden = false;
			
			if (info.type == .polarDay)
			{
				polarDayNightImage.image = polarDayIcon;
				polarDayNightLabel.text = L10n.polarDay;
			}
			else
			{
				polarDayNightImage.image = polarNightIcon;
				polarDayNightLabel.text = L10n.polarNight;
			}
		}
		
		setWeather();
	}
	

}

