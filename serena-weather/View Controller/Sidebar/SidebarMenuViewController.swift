//
//  SidebarMenuViewController.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//


import Foundation;
import UIKit;
class SidebarMenuController: UIViewController
{
	override func viewDidLoad()
	{
		super.viewDidLoad();	
	}
	
	@IBAction func findLocation(_ sender: UITapGestureRecognizer)
	{
		self.revealViewController().revealToggle(self);
		SWglobal.mainModel.NavigateToFindLocation();
	}
}
