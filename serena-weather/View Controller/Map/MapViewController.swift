//
//  MapViewController.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//


import Foundation;
import UIKit;
import MapKit;

class MapViewController: UIViewController, MKMapViewDelegate
	
{
	//MARK: outlets
	@IBOutlet weak var mapView: MKMapView!
	//MARK: properties
	var ininitalLocation: LocationModel? = nil;
	
	var resultSearchController: UISearchController? = nil;
	var selectedPin: MKPlacemark? = nil;
	
	var searchBar:UISearchBar? = nil;
	
//    var saveLocationButtonItem: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(menuButtonTapped(_:)));
	var saveLocationButtonItem: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(menuButtonTapped(_:)));
    
    @objc func menuButtonTapped(_ button:UIBarButtonItem)
	{
		if (selectedPin == nil)
		{
			return;
		}
		
		let toAdd = LocationModel(latitude: selectedPin!.coordinate.latitude, longitude: selectedPin!.coordinate.longitude, description: selectedPin!.name, zip: selectedPin!.postalCode);	SWglobal.mainModel.addLocation(toAdd, save: true);
		navigationController?.popViewController(animated: true);
	}
	
	override func viewDidLoad()
	{
		super.viewDidLoad();
		
		 saveLocationButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(menuButtonTapped(_:)));
		
		let locationSearchTable = storyboard!.instantiateViewController(withIdentifier: "LocationSearchTable") as! LocationSearchTable
		
		resultSearchController = UISearchController(searchResultsController: locationSearchTable);
		resultSearchController?.searchResultsUpdater = locationSearchTable;
		
		searchBar = resultSearchController!.searchBar;
		searchBar!.sizeToFit();
		searchBar!.placeholder = L10n.search;
		navigationItem.titleView = resultSearchController?.searchBar;
		
		self.navigationItem.rightBarButtonItem = saveLocationButtonItem;

		resultSearchController?.hidesNavigationBarDuringPresentation = false;
		resultSearchController?.dimsBackgroundDuringPresentation = true;
		definesPresentationContext = true;
		
		locationSearchTable.mapView = mapView;
		locationSearchTable.handleMapSearchDelegate = self;
	}
	
	
	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated);
		
		saveLocationButtonItem.isEnabled = false;
		
		if (ininitalLocation != nil)
		{
			let coordinate = CLLocationCoordinate2D(latitude: ininitalLocation!.latitude, longitude: ininitalLocation!.longitude);
			
			let span = MKCoordinateSpanMake(1, 1);
			let region = MKCoordinateRegion(center: coordinate, span: span);
			
			let annotation = MKPointAnnotation();
			annotation.coordinate = coordinate;
			annotation.title = ininitalLocation?.name;
			mapView.addAnnotation(annotation);
			
			mapView.setRegion(region, animated: true);
			
		}
	}
	
	
}

extension MapViewController: HandleMapSearch
{
	func dropPinZoomIn(placemark:MKPlacemark)
	{
		searchBar?.text = placemark.name;
		
		// cache the pin
		selectedPin = placemark;
		// clear existing pins
		mapView.removeAnnotations(mapView.annotations);
		
		let annotation = MKPointAnnotation();
		
		annotation.coordinate = placemark.coordinate;
		annotation.title = placemark.name;
		
		if let city = placemark.locality,
			let state = placemark.administrativeArea
		{
			annotation.subtitle = "\(city) \(state)";
		}
		
		mapView.addAnnotation(annotation);
		let span = MKCoordinateSpanMake(0.05, 0.05);
		let region = MKCoordinateRegionMake(placemark.coordinate, span);
		mapView.setRegion(region, animated: true);
		
		saveLocationButtonItem.isEnabled = !SWglobal.mainModel.containsLocation(placeMark: placemark);
	}
}

protocol HandleMapSearch
{
	func dropPinZoomIn(placemark: MKPlacemark);
}
