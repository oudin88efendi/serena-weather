// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

import Foundation

enum L10n {
  
  /// checking weather forecast...
  static let checkingForecast = L10n.tr("checking_forecast")
  /// weather forecast unavailable
  static let forecastUnavailable = L10n.tr("forecast_unavailable")
  /// from
  static let from = L10n.tr("from")
  /// polar day
  static let polarDay = L10n.tr("polar_day")
  /// polar night
  static let polarNight = L10n.tr("polar_night")
  /// search...
  static let search = L10n.tr("search")
  
  static let to = L10n.tr("to")
  
  /// Warsaw
  static let warsaw = L10n.tr("Warsaw")
}

extension L10n {
  fileprivate static func tr(_ key: String, _ args: CVarArg...) -> String {
    let format = NSLocalizedString(key, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}

// swiftlint:enable type_body_length
// swiftlint:enable nesting
// swiftlint:enable variable_name
// swiftlint:enable valid_docs
