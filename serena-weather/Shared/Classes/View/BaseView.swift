//
//  BaseView.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//

import UIKit

class BaseView: UIView {
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    
    let vwBase = self.getViewFromXib()
    
    if let _vwBase: UIView = vwBase {
      self.addSubview(_vwBase)
    }
  }
}

