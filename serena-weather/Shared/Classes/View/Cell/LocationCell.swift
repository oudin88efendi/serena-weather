//
//  LocationCell.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//


import Foundation;
import UIKit;

class LocationCell : UICollectionViewCell
{
	
	@IBOutlet weak var locationNameLabel: UILabel!;
	
    static let locationNameFont:UIFont = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.thin);

	var locationName: String
	{
		didSet
		{
			locationNameLabel.text = locationName;
		}
	}
	
	override var isSelected: Bool
	{
		didSet
		{
			locationNameLabel.textColor = isSelected ? UIColor.yellow : UIColor.white;
		}
	}


	required init?(coder aDecoder: NSCoder)
	{
		self.locationName = "";
		super.init(coder: aDecoder);
	}
}
