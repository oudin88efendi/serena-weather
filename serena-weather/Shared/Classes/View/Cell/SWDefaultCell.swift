//
//  SWDefaultCell.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//

import UIKit

class SWDefaultCell: UITableViewCell {
  // MARK: - OUTLETS
  @IBOutlet weak var lblTitle: UILabel!
  
  // MARK: - ATTRIBUTES
  
  // MARK: - LIFECYCLE
  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
