//
//  SWTableHeader.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//

import UIKit

class SWTableHeader: UIView {
  
  // MARK: - OUTLETS
  @IBOutlet weak var lblTitle: UILabel!
  @IBOutlet weak var lblRightTitle: UILabel!
  @IBOutlet weak var vwBorderBottom: UIView!
  
  // MARK: - ATTRIBUTES
  
  // MARK: - LIFECYLE
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
  }
  
}
