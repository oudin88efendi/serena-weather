//
//  SWHeader.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//

import UIKit

enum SWHeaderBtn {
  case left
  case right
}

protocol SWHeaderDelegate: NSObjectProtocol {
  func headerAction(fromBtn btn: SWHeaderBtn)
}

class SWHeader: BaseView {
  // MARK: - OUTLETS
  @IBOutlet weak var lblTitle: UILabel!
  @IBOutlet weak var btnLeft: UIButton! {
    didSet {
      if (self.btnLeft.currentTitle ?? "").isEmpty {
        self.btnLeft.isHidden = true
      }else {
        self.btnLeft.isHidden = false
      }
    }
  }

  @IBOutlet weak var btnRight: UIButton! {
    didSet {
      if (self.btnRight.currentTitle ?? "").isEmpty {
        self.btnRight.isHidden = true
      }else {
        self.btnRight.isHidden = false
      }
    }
  }
  
  // MARK: - ATTRIBUTES
  weak var delegate: SWHeaderDelegate!
  
  @IBInspectable
  var bgColor: UIColor = .clear {
    didSet {
      self.backgroundColor = self.bgColor
    }
  }
  // MARK: - LIFECYCLE
  override func awakeFromNib() {
    super.awakeFromNib()
    self.btnRight.setTitle("", for: .normal)
    self.btnLeft.setTitle("", for: .normal)
    
  }
  
  func setupUI(withTitle strTitle: String, btnLeftTitle: String = "", btnRightTitle: String = "", isPopUp: Bool = false) {
    self.lblTitle.text = strTitle
    self.btnLeft.setTitle(btnLeftTitle, for: .normal)
    self.btnRight.setTitle(btnRightTitle, for: .normal)
    
    if btnLeftTitle.isEmpty {
      self.btnLeft.isHidden = true
    }else {
      self.btnLeft.isHidden = false
    }
    
    if isPopUp {
      self.btnLeft.setImage(nil, for: .normal)
      self.btnLeft.contentEdgeInsets.right = 0
      self.btnLeft.titleEdgeInsets.left = 0
      self.btnLeft.titleEdgeInsets.right = 0
    }
  }
  
  // MARK: - ACTIONS
  @IBAction func actBtnLeft(_ sender: Any) {
    self.delegate.headerAction(fromBtn: .left)
  }
  
  @IBAction func actBtnRight(_ sender: Any) {
    self.delegate.headerAction(fromBtn: .right)
  }
  
}
