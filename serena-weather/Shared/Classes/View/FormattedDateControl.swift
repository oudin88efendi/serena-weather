//
//  FormattedDateControl.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//

import UIKit

@IBDesignable class FormattedDateControl: UIStackView
{

	override init(frame: CGRect)
	{
		super.init(frame: frame);
		setView();
	}
	
	required init(coder: NSCoder)
	{
		super.init(coder: coder);
		setView();
	}
	
	func setView()
	{
		dayOfWeekLabel.textColor = UIColor.white;
		dayOfWeekLabel.font = UIFont.systemFont(ofSize: 25, weight: UIFont.Weight.bold);
		monthLabel.textColor = UIColor.white;
		dayLabel.textColor = UIColor.white;
		dayLabel.font = UIFont.systemFont(ofSize: 25, weight: UIFont.Weight.thin);
//        addArrangedSubview(dayOfWeekLabel);
        
		let verticalStack = UIStackView();
		verticalStack.axis = UILayoutConstraintAxis.vertical;
		verticalStack.addArrangedSubview(dayOfWeekLabel);
		verticalStack.addArrangedSubview(monthLabel);
		verticalStack.addArrangedSubview(dayLabel);
        
		
		addArrangedSubview(verticalStack);
	}
	
	func formatDate()
	{
		let dateFormatter = DateFormatter();
		
		dateFormatter.dateFormat = "MMM";
		month = dateFormatter.string(from: date).uppercased();
		
		dateFormatter.dateFormat = "dd";
		day = dateFormatter.string(from: date);
		
		dateFormatter.dateFormat = "EEEE";
		dayOfWeek = dateFormatter.string(from: date).replacingOccurrences(of: ".", with: "").trim(2).uppercased();

	}
	
	//MARK: properties
	var dayLabel:UILabel = UILabel();
	var monthLabel:UILabel = UILabel();
	var dayOfWeekLabel:UILabel = UILabel();
	
	@IBInspectable var dayOfWeek:String = "MON"
	{
		didSet
		{
			dayOfWeekLabel.text = dayOfWeek;
		}
	}
	
	@IBInspectable var day:String = "07"
	{
		didSet
		{
			dayLabel.text = day;
		}
	}
	
	@IBInspectable var month:String = "OCT"
	{
		didSet
		{
			monthLabel.text = month;
		}
	}

	@IBInspectable var date:Date = Date()
	{
		didSet
		{
			formatDate();
		}
	}
}
