//
//  SWSecurity.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//

import Foundation
import UIKit
import CryptoSwift
import JWT
import SwiftyRSA

class SWSecurity: NSObject {
  
  static func getToken(withParam param: [String:Any]) -> [String:Any] {
    let token = JWT.encode(claims: param, algorithm: .hs256("YOUR DATA".data(using: .utf8)!))
  
    return ["token": token]
  }
  
  static func decryptToken() {
    do {
      let claims: ClaimSet = try JWT.decode("YOUR_JWT_TOKEN", algorithm: .hs256("secret".data(using: .utf8)!))
      print(claims)
    } catch {
      print("Failed to decode JWT: \(error)")
    }
  }
  
  static func encryptCredential(from strString: String) -> String {
    let publicKey = try! PublicKey(derNamed: "YOUR_PUBLIC_DER_NAME")
    let _password = try! ClearMessage(string: "\(strString)", using: .utf8)
    let encrypted = try! _password.encrypted(with: publicKey, padding: .PKCS1)
    
    return encrypted.base64String
  }
}
