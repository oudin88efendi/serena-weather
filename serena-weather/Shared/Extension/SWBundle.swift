//
//  SWBundle.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//

import Foundation

extension Bundle {
  var releaseVersionNumber: String {
    return infoDictionary?["CFBundleShortVersionString"] as? String ?? "1.0.0"
  }
  var buildVersionNumber: String {
    return infoDictionary?["CFBundleVersion"] as? String ?? "1"
  }
}
