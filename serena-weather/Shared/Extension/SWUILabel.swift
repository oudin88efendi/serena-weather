//
//  SWUILabel.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//

import UIKit

extension UILabel {
  
    func setLineHeight(lineHeight: CGFloat) {
        let text = self.text
        if let text = text {
            let attributeString = NSMutableAttributedString(string: text)
            let style = NSMutableParagraphStyle()
            
            style.lineSpacing = lineHeight
            attributeString.addAttribute(NSAttributedStringKey.paragraphStyle, value: style, range: NSMakeRange(0, text.count))
            self.attributedText = attributeString
        }
    }
    
  
  func setRelativeFontSize() {
    let screeWidth: CGFloat = UIScreen.main.bounds.width
    let relativeFontSize: CGFloat = (screeWidth/375)*self.font.pointSize
    self.font = UIFont(name: self.font.fontName, size: relativeFontSize)
  }
  
  func isLoading(_ isStart: Bool) {
    if isStart {
      let width: CGFloat = 30.0
      let height: CGFloat = 30.0
      let y: CGFloat = (self.bounds.height/2) - (height/2)
      var x = (self.bounds.width/2) - (width/2)
      
      switch self.textAlignment {
      case .left:
        x = 0.0
      case .right:
        x = self.bounds.width - width
      default:
        break
      }
      
      
      let lblCurrent = UILabel()
      lblCurrent.textColor = self.textColor
      lblCurrent.tag = 90
      
      self.textColor = .clear
      
      let vwLoadingIndicator = UIActivityIndicatorView(frame: CGRect(x: x, y: y, width: width, height: height))
      vwLoadingIndicator.tag = 99
      vwLoadingIndicator.startAnimating()
      
      self.addSubview(vwLoadingIndicator)
      self.addSubview(lblCurrent)
      
    } else {
      guard
        let vwLoadingIndicator = self.viewWithTag(99),
        let lblCurrent = self.viewWithTag(90),
        let _lblCurrent = lblCurrent as? UILabel
      else {return}
      
      vwLoadingIndicator.removeFromSuperview()
      self.textColor = _lblCurrent.textColor
      
    }
  }
  
  
}


