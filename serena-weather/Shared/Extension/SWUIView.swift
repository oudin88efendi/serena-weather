//
//  SWUIView.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//

import UIKit

//MARK: -
extension UIView {
  //MARK: - Initialization
  static func getViewFromClassNib(bundle bdl: Bundle? = nil, boundsNew rectBoundsNew: CGRect) -> UIView? {
    return self.getViewFromClassXib(bundle: nil, restorationId: nil, boundsNew: rectBoundsNew)
  }
  
  static func getViewFromClassXib(bundle bdl: Bundle? = nil, restorationId strRestorationId: String? = nil, boundsNew rectBoundsNew: CGRect) -> UIView? {
    let nibVw: UINib
    
    if let _bdl = bdl {
      nibVw = UINib(nibName: String(describing: self.classForCoder()), bundle: _bdl)
    } else {
      nibVw = UINib(nibName: String(describing: self.classForCoder()), bundle: Bundle(for: self.classForCoder()))
    }
    
    let arrVw: [UIView]? = nibVw.instantiate(withOwner: self, options: nil) as? [UIView]
    
    var vwRequested: UIView? = nil
    
    if let _arrVw = arrVw {
      for vw in _arrVw {
        if let _strRestorationId = strRestorationId,
          let __strRestoratonId = vw.restorationIdentifier {
          if _strRestorationId.compare(__strRestoratonId) == ComparisonResult.orderedSame {
            vwRequested = vw
          }
        } else {
          if vw.isKind(of: self.classForCoder()) {
            vwRequested = vw
          }
        }
      }
    }
    
    if let _vwRequested = vwRequested {
      _vwRequested.frame = rectBoundsNew
      
      _vwRequested.layoutIfNeeded()
    }
    
    return vwRequested
  }
  
  func getViewFromXib(bundle bdl: Bundle? = nil) -> UIView? {
    return self.getViewFromXib(bundle: nil, restorationId: nil)
  }
  
  func getViewFromXib(bundle bdl: Bundle? = nil, restorationId strRestorationId: String?) -> UIView? {
    let nibVw: UINib
    
    if let _bdl = bdl {
      nibVw = UINib(nibName: String(describing: self.classForCoder), bundle: _bdl)
    } else {
      nibVw = UINib(nibName: String(describing: self.classForCoder), bundle: Bundle(for: self.classForCoder))
    }
    
    let arrVw: [UIView]? = nibVw.instantiate(withOwner: self, options: nil) as? [UIView]
    
    var vwRequested: UIView? = nil
    
    if let _arrVw = arrVw {
      for vw in _arrVw {
        if let _strRestorationId = strRestorationId,
          let __strRestoratonId = vw.restorationIdentifier {
          if _strRestorationId.compare(__strRestoratonId) == ComparisonResult.orderedSame {
            vwRequested = vw
          }
        } else {
          vwRequested = vw
        }
      }
    }
    
    if let _vwRequested = vwRequested {
      _vwRequested.frame = self.bounds
    }
    
    return vwRequested
  }
  
  func shake() {
    let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
    animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
    animation.duration = 0.5
    animation.values = [-15.0, 15.0, -15.0, 15.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
    layer.add(animation, forKey: "shake")
  }
  
  // MARK: - UI

  func dropShadow(scale: Bool = true) {
    self.layer.masksToBounds = false
    self.layer.shadowColor = UIColor.black.cgColor
    self.layer.shadowOpacity = 0.5
    self.layer.shadowOffset = CGSize(width: -1, height: 1)
    self.layer.shadowRadius = 1
    
    self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
    self.layer.shouldRasterize = true
    self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
  }
  
  func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
    self.layer.masksToBounds = false
    self.layer.shadowColor = color.cgColor
    self.layer.shadowOpacity = opacity
    self.layer.shadowOffset = offSet
    self.layer.shadowRadius = radius
  
    self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds,cornerRadius: self.layer.cornerRadius).cgPath
    self.layer.shouldRasterize = true
    self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
  }
  
}

