//
//  SWDouble.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//

import Foundation
extension Double
{
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double
    {
        let divisor = pow(10.0, Double(places));
        return (self * divisor).rounded() / divisor;
    }
}
