//
//  SWCell.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//

import UIKit

//MARK: - Cell Identifier
protocol ReuseIdentifying {
  static var reuseIdentifier: String { get }
}

extension ReuseIdentifying {
  static var reuseIdentifier: String {
    return String(describing: Self.self)
  }
}

extension UICollectionViewCell: ReuseIdentifying {}
extension UITableViewCell: ReuseIdentifying {}
