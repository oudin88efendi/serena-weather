//
//  SWNotification.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//

import UIKit

extension Notification.Name {
  static let tokenExpired = Notification.Name(rawValue: "TOKEN_EXPIRED")
  static let networkError = Notification.Name(rawValue: "NETWORK_ERROR")
}
