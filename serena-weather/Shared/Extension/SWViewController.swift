//
//  SWViewController.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
  func performBack() {
    if let nav = self.navigationController {
      nav.popViewController(animated: true)
    } else {
      self.dismiss(animated: true, completion: nil)
    }
  }
}
