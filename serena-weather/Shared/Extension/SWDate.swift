//
//  SWDate.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//
import Foundation

extension Date
{
    func dateOnlyString() -> String
    {
        let timeFormatter = DateFormatter();
        timeFormatter.dateStyle = .medium;
        timeFormatter.timeStyle = .none;
        
        return timeFormatter.string(from: self);
    }
}
