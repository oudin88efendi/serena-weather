//
//  SWString.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//

import Foundation
import UIKit

extension String {
  var hex: Int! {
    return Int(self, radix: 16)
  }
    
   func trim(_ charactersCount: Int) -> String
    {
        if (self.characters.count <= charactersCount)
        {
            return self;
        }
        
        let start = self.startIndex;
        let end = self.index(self.endIndex, offsetBy: 0 - charactersCount + 1);
        return String(self[start..<end]);
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat
    {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude);
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil);
        return boundingBox.height;
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat
    {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height);
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        return boundingBox.width;
    }
  
  func isValidEmail() -> Bool {
    // here, `try!` will always succeed because the pattern is valid
    let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
    return emailPredicate.evaluate(with: self)
  }
  
  func getInitials() -> String {
    let initials = self.components(separatedBy: " ").reduce("") { ($0 == "" ? "" : "\($0.first!)") + "\($1.first!)" }
    
    return initials
  }
  
  func isPhoneNumber() -> Bool {
    let PHONE_REGEX =  "[235689][0-9]{5}([0-9]{3})"

    do {
      let regex = try NSRegularExpression(pattern: PHONE_REGEX)
      let results = regex.matches(in: self,
                                  range: NSRange(self.startIndex..., in: self))
      
      if !results.isEmpty {
        return true
      }
      return false
    } catch let error {
      print("invalid regex: \(error.localizedDescription)")
      return false
    }
  }
  
  func formatedDate(format strOriginFormat: String,strToFormat: String) -> String {
    let dateString = self
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = strOriginFormat
    dateFormatter.locale = Locale(identifier: "id_ID")

    
    guard let dateObj = dateFormatter.date(from: dateString) else {return ""}
    
    dateFormatter.dateFormat = strToFormat
    
    return dateFormatter.string(from: dateObj)
  }
  
  //MARK: - Currency
  private func generateAbsoluteCurrency() -> String {
    let charSetSeparator: CharacterSet = CharacterSet(charactersIn: ".")

    let arrStrComponent: [String] = self.components(separatedBy: charSetSeparator)
    
    guard let _strAbsoluteValue: String = arrStrComponent.first else {
      return ""
    }
    
    return _strAbsoluteValue
  }
  
  func localCurrency() -> String {
    
    let localeCurrent: Locale = Locale(identifier: "id_ID")
    let numFormatter: NumberFormatter = NumberFormatter()
    numFormatter.numberStyle = NumberFormatter.Style.decimal
    numFormatter.maximumFractionDigits = 2
    numFormatter.locale = localeCurrent
    
    let num: NSNumber
    if let _dblValue: Double = Double(self.generateAbsoluteCurrency()) {
      num = NSNumber(value: Double(_dblValue))
    } else {
      let dblValue: Double = NSString(string: self.generateAbsoluteCurrency()).doubleValue
      
      num = NSNumber(value: dblValue)
    }
    
    guard let _strNewValue: String = numFormatter.string(from: num) else {
      return self.generateAbsoluteCurrency()
    }
    
    if let _strCurrencySymbol: String = localeCurrent.currencySymbol {
      return "\(_strCurrencySymbol) \(_strNewValue)"
    } else if let _strCurrencySymbol: String = (localeCurrent as NSLocale).object(forKey: NSLocale.Key.currencySymbol) as? String {
      return "\(_strCurrencySymbol) \(_strNewValue)"
    } else {
      return self.generateAbsoluteCurrency()
    }
  }
  
  func sanitiseForNumber() -> String {
    let characterSet = Set("1234567890")
    return self.filter { characterSet.contains($0)};
  }
}
