//
//  SWUIColor.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
  convenience init(hex: Int) {
    self.init(hex: hex, a: 1.0)
  }
  
  convenience init(hex: Int, a: CGFloat) {
    self.init(r: (hex >> 16) & 0xff, g: (hex >> 8) & 0xff, b: hex & 0xff, a: a)
  }
  
  convenience init(r: Int, g: Int, b: Int) {
    self.init(r: r, g: g, b: b, a: 1.0)
  }
  
  convenience init(r: Int, g: Int, b: Int, a: CGFloat) {
    self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: a)
  }
  
  convenience init(hexString: String) {
    self.init(hex: hexString.hex)
  }
  
  convenience init(hexString: String, a: CGFloat) {
    self.init(hex: hexString.hex, a: a)
  }
}
