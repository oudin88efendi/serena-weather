//
//  DMMenu.swift
//  project
//
//  Created by Alfian on 1/29/18.
//  Copyright © 2018 DIGITAL ARTHA MEDIA, PT. All rights reserved.
//

import UIKit
import SwiftyJSON

struct DMMenu {
  var strId: String = ""
  var strType: String = ""
  var strTitle: String = ""
  var strDescription: String = ""
  
  init(json: JSON) {
    self.strId = json["id"].stringValue
    self.strType = json["type"].stringValue
    self.strTitle = json["title"].stringValue
    self.strDescription = json["description"].stringValue
  }
}

