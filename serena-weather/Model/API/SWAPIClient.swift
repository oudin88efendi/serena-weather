//
//  SWAPIClient.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

typealias resultHendler = (JSON?,Error?)

class SWAPIClient: NSObject {
  func stringFromTime(interval: TimeInterval) -> String {
    let ms = Int(interval.truncatingRemainder(dividingBy: 1) * 1000)
    let formatter = DateComponentsFormatter()
    formatter.allowedUnits = [.hour, .minute, .second]
    return formatter.string(from: interval)! + ".\(ms)"
  }
  

  // This method hendle request using Alamofire
  func request(param: [String: Any], httpMethod: Alamofire.HTTPMethod,paramEncoding: ParameterEncoding = JSONEncoding.default, strURL: String, requestHendler: @escaping (resultHendler) -> Void) {
    
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
    
    var requestResult: (jsonData: JSON?, error: Error?)
    
    
    let headers: HTTPHeaders = [:]
    
    //REQUEST
    Alamofire.request(strURL, method: httpMethod, parameters: param, encoding: paramEncoding, headers: headers)
      .responseJSON { response in
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        print(response.request as Any)  // original URL request
        print(response.response as Any) // URL response
        print(response.result.value as Any)   // result of response serialization
        print("\nRESPONSE TIME")
        print(response.timeline)
        
        switch response.result {
        case .success:
          if let data = response.result.value {
            requestResult.jsonData = JSON(data)
            requestHendler(requestResult)
          }
        case .failure(let _error):
          requestResult.error = _error
          requestHendler(requestResult)
          self.showError(response: response.response)
        }
    }
  }
  
  func request(httpMethod: Alamofire.HTTPMethod , strURL: String, requestHendler: @escaping (resultHendler) -> Void) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
    var requestResult: (jsonData: JSON?, error: Error?)
    
    Alamofire.request(strURL).responseJSON { response in
      UIApplication.shared.isNetworkActivityIndicatorVisible = false
      debugPrint(response)
      
      switch response.result {
      case .success:
        if let data = response.result.value {
          requestResult.jsonData = JSON(data)
          requestHendler(requestResult)
        }
      case .failure(let _error):
        requestResult.error = _error
        requestHendler(requestResult)
        self.showError(response: response.response)
      }
    }
  }
  
  private func showError(response URLResponse: HTTPURLResponse?) {
    DispatchQueue.main.async {
      guard let window = UIApplication.shared.keyWindow else {return}
      if let HTTPResponse = URLResponse {
        window.makeToast("ERROR: \(HTTPResponse.statusCode)", position: .top)
      }
    }
  }
    
    
    static func fetch(lat: Double, lon: Double, completion: @escaping ([String: WeatherModel]) -> Void)
    {
        let urlString = SWglobal.getWeatherUrl(lat: lat, lon: lon);
        let url = URL(string: urlString);
        var days = [String: WeatherModel]();
        
        URLSession.shared.dataTask(with:url!)
        {
            (data, _, error) in
            
            if error != nil
            {
                print(error!);
            }
            else
            {
                do
                {
                    let parsedData = try JSONSerialization.jsonObject(with: data!, options: []) as! [String:Any];
                    
                    let dailyConditions = parsedData["daily"] as! [String:AnyObject];
                    let daysData = dailyConditions["data"] as! [AnyObject];
                    
                    for case let dayData in daysData
                    {
                        let time = dayData["time"] as! Double;
                        let date = Date(timeIntervalSince1970: time);
                        
                        let icon = dayData["icon"] as! String;
                        let summary = dayData["summary"] as! String;
                        let tempMin = dayData["temperatureMin"] as! Double;
                        let tempMax = dayData["temperatureMax"] as! Double;
                        
                        days[date.dateOnlyString()] = WeatherModel(date:date, icon:icon, summary: summary, tempMin: tempMin, tempMax:tempMax);
                        
                    }
                    
                }
                catch let error as NSError
                {
                    print(error);
                }
            }
            
            completion(days);
            
            }.resume()
    }
  
}
