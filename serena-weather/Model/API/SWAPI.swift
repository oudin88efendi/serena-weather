//
//  SWAPI.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//


import Foundation
import SwiftyJSON
import UIKit
import Alamofire

//MARK: -
class SWAPI {
  //MARK: - Attributes
  #if DEV
  private static let BASE_URL: String = "xxx"

  #else
  private static let BASE_URL: String = "xx"

  #endif
  
  private static let apiClient = SWAPIClient()
  
  //MARK: - CallBacks
  typealias callBackError = (_ message: String, _ code: String) -> Void
  
  
  //MARK: - Helper
  static func cancelAllRequest() {
    let sessionManager = Alamofire.SessionManager.default
    sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
      dataTasks.forEach { $0.cancel() }
      uploadTasks.forEach { $0.cancel() }
      downloadTasks.forEach { $0.cancel() }
      
    }
  }
  
  private static func validateResponse(json: JSON, callback: (_ isValid: Bool, _ message: String) -> Void) {
    let strStatus: String = json["status"].stringValue
    var strDescription: String = json["description"].stringValue
    
    if strDescription.isEmpty {
      strDescription = strStatus
    }
    
    if strStatus.isEmpty {
      callback(false, strDescription)
    } else {
      switch strStatus {
      case "VALID":
        callback(true, strDescription)
      case "TOKEN_EXPIRED":
        NotificationCenter.default.post(name: .tokenExpired, object: nil)
        callback(true, strDescription)
//        dunkin_coffee
      default:
         callback(true, strDescription)
      }
    }
  }
  
  // MARK: - API
  
 
  
}
