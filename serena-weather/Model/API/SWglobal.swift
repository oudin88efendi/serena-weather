//
//  SWglobal.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//


import Foundation

struct SWglobal
{
	static let mainModel:MainModel = MainModel();
	static let goldenHourDurationMinutes:Double = 30;
	static let blueHourDurationMinutes:Double = 30;
	
	static let weatherMapUrl = "https://api.darksky.net/forecast";
	static let weatherLang = "en";
    static let weatherMapApiKey =  "e2d7eecb8ac61991cd85654c4a8a703b";

	static let supportedWeatherLangs =
	[
		"ar",
		"az",
		"be",
		"bg",
		"bs",
		"ca",
		"cs",
		"de",
		"el",
		"en",
		"es",
		"et",
		"fr",
		"hr",
		"hu",
		"id",
		"it",
		"is",
		"kw",
		"nb",
		"nl",
		"pl",
		"pt",
		"ru",
		"sk",
		"sl",
		"sr",
		"sv",
		"tet",
		"tr",
		"uk",
		"x-pig-latin",
		"zh",
		"zh-tw"
	];
	
	static func getWeatherLang() -> String
	{
		if (Locale.current.languageCode == nil)
		{
			return weatherLang;
		}
		
		if (supportedWeatherLangs.contains(Locale.current.languageCode!))
		{
			return Locale.current.languageCode!;
		}
		
		return weatherLang;

	}
	
	static func getWeatherUnits() -> String
	{
		if (Locale.current.usesMetricSystem)
		{
			return "si";
		}
		
		return	"us";
	}
	
	static func getWeatherUrl(lat: Double, lon: Double) -> String
	{
        
		return "\(weatherMapUrl)/\(weatherMapApiKey)/\(lat),\(lon)?exclude=currently,minutely,hourly,alerts,flags&lang=\(getWeatherLang())&units=\(getWeatherUnits())";
	}
	
	static func getTemperatureUnits() -> String
	{
		if (getWeatherUnits() == "si")
		{
			return "°C";
		}
		
		return "°F";
	}
}
	
