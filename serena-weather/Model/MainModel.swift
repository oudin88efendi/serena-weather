//  MainViewModel.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//

import Foundation;
import CoreLocation;
import MapKit;

typealias GetCurrentLocationCompletion = (_ result: Bool) -> ()
typealias NavigateToFindLocationDelegate = () -> ()
typealias UpdateViewForLocationDelegate = (_ index: Int?) -> ()

class MainModel : NSObject, CLLocationManagerDelegate
{
	var navigateToFindLocationDelegate: NavigateToFindLocationDelegate? = nil;
	var updateViewForLocationDelegate: UpdateViewForLocationDelegate? = nil;
	
	func NavigateToFindLocation()
	{
		if navigateToFindLocationDelegate != nil
		{
			navigateToFindLocationDelegate!();
		}
	}
	
	var currentLocation: LocationModel? = nil;
	var locationCompletion: GetCurrentLocationCompletion? = nil;
	
	let locationManager = CLLocationManager();
	
	var dataSource: LocationDataSource? = nil;
		
	func containsLocation(placeMark:MKPlacemark) -> Bool
	{
		return containsLocation(zip: placeMark.postalCode, lat: placeMark.coordinate.latitude, lon: placeMark.coordinate.longitude);
	}
	
	func addLocation(_ location: LocationModel, save: Bool) -> Bool
	{
		self.currentLocation = location;
		
		if (containsLocation(zip: location.zip, lat: location.latitude, lon: location.longitude))
		{
			return false;
		}
		
		let index = dataSource!.addLocation(location);
		
		if (updateViewForLocationDelegate != nil)
		{
			updateViewForLocationDelegate!(index);
		}
		
		if (save)
		{
			saveLocations();
		}
		
		return true;
	}
	
	func containsLocation(zip: String?, lat: Double, lon: Double) -> Bool
	{
		return dataSource!.locations.first(where: { ($0.zip != nil && $0.zip == zip) || ($0.latitude == lat && $0.longitude == lon)}) != nil
	}
	
	func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
	{
		print("location error");
		locationCompletion!(false);
	}
	
	func getLocation(completion: @escaping GetCurrentLocationCompletion)
	{
		self.locationCompletion = completion;
		
		locationManager.delegate = self;
		locationManager.desiredAccuracy = kCLLocationAccuracyBest;
		locationManager.requestWhenInUseAuthorization();
		locationManager.requestLocation();
	}
	
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
	{
		print("Got user location");
		locationManager.stopUpdatingLocation();
		let userLocation:CLLocation = locations[0];
		CLGeocoder().reverseGeocodeLocation(userLocation)
		{ (placemarks, error) in
			
			var city: String?;
			var zip: String?;
			
			var addNew: Bool = true;
			
			// Check for errors
			if (error != nil)
			{
				print(error ?? "Unknown Error");
			}
			else
			{
				
				// Get the first placemark from the placemarks array.
				// This is your address object
				if let placemark = placemarks?[0]
				{
					city = placemark.locality;
					zip = placemark.postalCode;
					
					if (self.containsLocation(zip: zip, lat: userLocation.coordinate.latitude, lon:userLocation.coordinate.longitude))
					{
						addNew = false;
					}
				}
				
			}
			
            print("\(String(describing: zip)) \(String(describing: city))");
			
			let location = LocationModel(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude, description: city, zip: zip);
			
			self.addLocation(location, save: false);
			
			
			self.locationCompletion!(true);
		}
		
	}
	
	override init()
	{
		super.init();
		
	}
	
	func getLocations(delegate: @escaping SetLocationDelegate)
	{
		var locations: [LocationModel] = [];
		
		if let savedLocations = loadLocations()
		{
			locations = savedLocations;
		}
		else
		{
			// Load the sample data.
			locations = loadSampleLocations();
		}
		
		self.dataSource = LocationDataSource(locations: locations, delegate: delegate);

	}
	
	private func loadLocations() -> [LocationModel]?
	{
		return NSKeyedUnarchiver.unarchiveObject(withFile: LocationModel.ArchiveURL.path) as? [LocationModel];
	}
	
	func loadSampleLocations() -> [LocationModel]
	{
		
		let locations = [
			LocationModel(latitude: -6.121435, longitude: -149.863129, description: "Jakarta", zip: nil),
			LocationModel(latitude: -6.595038, longitude: 106.816635, description: "Bogor", zip: nil),
            LocationModel(latitude: -6.385589, longitude: 106.830711, description: "Depok", zip: nil),
            LocationModel(latitude: -6.178306, longitude: 106.631889, description: "Tanggerang", zip: nil),
            LocationModel(latitude:  -6.178306, longitude: 106.992416, description: "Bekasi", zip: nil),
		];
		
		return locations;

	}   
	
	func saveLocations()
	{
		let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(dataSource!.locations, toFile: LocationModel.ArchiveURL.path);
		
		print("saved locations: \(isSuccessfulSave)");
	}
}
