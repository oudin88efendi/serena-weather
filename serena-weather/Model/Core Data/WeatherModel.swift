//
//  WeatherModel.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//


import Foundation
class WeatherModel
{
	
	let date: Date;
	let icon: String;
	let summary: String;
	let tempMin: Int;
	let tempMax: Int;
	let temperatureInfo: String;
	
	init(date: Date, icon: String, summary: String, tempMin: Double, tempMax: Double)
	{
		self.date = date;
		self.icon = icon;
		self.summary = summary;
		self.tempMin = Int(round(tempMin));
		self.tempMax = Int(round(tempMax));
		
		let units = SWglobal.getTemperatureUnits();
		
		self.temperatureInfo = "\(L10n.from) \(self.tempMin) \(units) \(L10n.to) \(self.tempMax) \(units)";
	
		print("\(date.dateOnlyString()), \(icon), \(summary), \(tempMin)-\(tempMax)");
	}
}
