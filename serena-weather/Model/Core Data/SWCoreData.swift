//
//  SWCoreData.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//


import Foundation
import UIKit
import CoreData
 
class SWCoreData: NSObject {
  
  // MARK: - INSERT
  static func insertContact(_ strName: String,strNumber: String, callback: @escaping (Bool) -> Void) {
    var dictAttribute: [String:Any] = [:]
    dictAttribute["strType"] = "CONTACT"
    dictAttribute["strName"] = strName
    dictAttribute["strNumber"] = strNumber
    
    callback(SWCoreDataHandler.saveObject(forEntity: SWContact(), withAttribute: dictAttribute))
  }
  
  // MARK: - FETCH
  static func fetchFavoriteTransction(filterBy strFilter:(key: String, value: String) = ("",""), callback: @escaping ([SWContact]) -> Void) {
   
    SWCoreDataHandler.fetchObject(fromEntity: SWContact.self, filterBy: strFilter, callback: { (result) in
      if let arrTransaction = result as? [SWContact] {
        callback(arrTransaction)
      }else if result is Dictionary<String, Any> {
        
      }else {
        callback([])
      }
      
    })
  }
    
  
}
