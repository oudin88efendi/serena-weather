//
//  SWCoreDataHandler.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class SWCoreDataHandler: NSObject {
    private class func getContext() -> NSManagedObjectContext {
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    class func saveObject(forEntity entity: NSManagedObject, withAttribute dictAttribute: [String:Any]) -> Bool {
        let context = getContext()
        
        guard let entity = NSEntityDescription.entity(forEntityName: String(describing: entity.classForCoder), in: context) else {
            print("Entity not found")
            return false
        }
        
        let manageObject = NSManagedObject(entity: entity, insertInto: context)
        
        for (key,value) in dictAttribute {
            manageObject.setValue(value, forKey: key)
        }
        
        do {
            try context.save()
            print("Save success")
            return true
        } catch{
            print("Failed to save object")
            return false
        }
    }
    
    class func fetchObject(fromEntity entity: AnyObject,filterBy strFilter: (key: String, value: String) = ("",""),groupBy strGroupKey: String = "", callback: @escaping (Any) -> Void) {
        let context = getContext()
        
        
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        guard let entity = NSEntityDescription.entity(forEntityName: String(describing: entity), in: context) else {
            print("Entity not found")
            return
        }
        
        // Configure Fetch Request
        fetchRequest.entity = entity
        if !strFilter.key.isEmpty {
            fetchRequest.predicate = NSPredicate(format: "%@ == %@", strFilter.key,strFilter.value)
        }
        
        if !strGroupKey.isEmpty {
            let keypathExp = NSExpression(forKeyPath: strGroupKey) // can be any column
            let expression = NSExpression(forFunction: "count:", arguments: [keypathExp])
            
            let countDesc = NSExpressionDescription()
            countDesc.expression = expression
            countDesc.name = "count"
            countDesc.expressionResultType = .integer64AttributeType
            
            fetchRequest.propertiesToFetch = [strGroupKey]
            fetchRequest.propertiesToGroupBy = [strGroupKey]
            fetchRequest.resultType = .dictionaryResultType
        }
        
        do {
            let result = try context.fetch(fetchRequest)
            dump(result)
            callback(result)
            
        } catch {
            let fetchError = error as NSError
            print(fetchError.localizedDescription)
            callback([])
            
        }
    }
    
    class func fetchResultController(fromEntity entity: AnyObject) -> NSFetchedResultsController<NSFetchRequestResult> {
        let context = getContext()
        let fetchResultController: NSFetchedResultsController<NSFetchRequestResult>!
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        // Create Entity Description
        if let entity = NSEntityDescription.entity(forEntityName: String(describing: entity), in: context){
            // Configure Fetch Request
            fetchRequest.entity = entity
            
            fetchResultController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
            
            return fetchResultController
        }else {
            print("Entity not found")
        }
        
        return NSFetchedResultsController()
    }
}
