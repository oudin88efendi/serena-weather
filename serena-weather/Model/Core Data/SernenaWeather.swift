//
//  SernenaWeather.swift
//  serena-weather
//
//  Created by NRD on 21/09/18.
//  Copyright © 2018 nrd88efendi. All rights reserved.
//

import Foundation
class SernenaWeather
{
	var SwTime:DateComponents;
	var SwDate:Date;
	
	let calendar = Calendar.current;
	let timeFormatter:DateFormatter;

	var formatted:String
	{
		return timeFormatter.string(from: SwDate);
	}
	
	
	init(date: Date)
	{
		timeFormatter = DateFormatter();
		timeFormatter.dateStyle = .none;
		timeFormatter.timeStyle = .short;
		SwDate = date;
		SwTime = calendar.dateComponents([.hour, .minute], from: SwDate);
	}
}
